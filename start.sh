# Launch script in background
flask run &
# Get its PID
PID=$!
# Wait for 2 seconds
sleep 10
# Kill it
kill $PID
